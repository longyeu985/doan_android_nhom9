package com.example.thigiuaky.thigiuaky;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Windows 10 TIMT on 5/11/2018.
 */

public class ctadapter extends ArrayAdapter<Country> {

    Activity context;
    int resource;
    @NonNull List<Country> objects;

    public ctadapter(@NonNull Activity context, int resource, @NonNull List<Country> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();

        View item = inflater.inflate(this.resource,parent,false);

        ImageView imglogo = item.findViewById(R.id.imgFlag);
        TextView txtname = item.findViewById(R.id.tv_name);
        TextView txtcontent = item.findViewById(R.id.tv_hanhdong);
        ImageView icon1 = item.findViewById(R.id.imgFlag2);
        ImageView icon2 = item.findViewById(R.id.imgFlag3);

        Country country = new Country();
        imglogo.setImageResource(this.objects.get(position).getAvarta());
        txtname.setText(this.objects.get(position).getName());
        txtcontent.setText(this.objects.get(position).getContent());
        icon1.setImageResource(this.objects.get(position).getIcon1());
        icon2.setImageResource(this.objects.get(position).getIcon2());

        return item;
    }
}
