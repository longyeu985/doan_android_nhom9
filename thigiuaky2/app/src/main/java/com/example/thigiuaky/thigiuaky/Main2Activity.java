package com.example.thigiuaky.thigiuaky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
public class Main2Activity extends AppCompatActivity {
    Button btn_register;
    Button btn_cancel;
    EditText edt_firstname;
    EditText edt_lastname;
    EditText edt_user;
    EditText edt_pass;
    EditText edt_pass2;
    EditText edt_phone;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Spinner spin_job;
    private static final String[] job = {"Shiper","Gamer","Designer","Teacher","Study"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        btn_register = (Button)findViewById(R.id.btn_register);
        btn_cancel = (Button)findViewById(R.id.btn_cancel);
        edt_firstname = (EditText)findViewById(R.id.edt_firstname);
        edt_lastname= (EditText)findViewById(R.id.edt_firstname);
        edt_pass = (EditText)findViewById(R.id.edt_firstname);
        edt_pass2 = (EditText)findViewById(R.id.edt_firstname);
        edt_phone = (EditText)findViewById(R.id.edt_firstname);
        edt_user = (EditText)findViewById(R.id.edt_firstname);
        spin_job = (Spinner)findViewById(R.id.spin_job);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        ArrayAdapter<String> adapter;
       adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.itemspin,job);
        spin_job.setAdapter(adapter);
        spin_job.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String edtuser = edt_user.getText().toString().trim();
                String edtpass = edt_pass.getText().toString().trim();
                String edtpass2 = edt_pass2.getText().toString().trim();
                String firstname = edt_firstname.getText().toString().trim();
                String lastname = edt_lastname.getText().toString().trim();
                String phone = edt_phone.getText().toString().trim();
                if(firstname.equals("")&&lastname.equals("")&&edtuser.equals("")&&edtpass.equals("")&&edtpass2.equals("")&&phone.equals("")){
                    Toast.makeText(getApplicationContext(), "Vui long Nhap day du tat ca thong tin", Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    class User{
        String username;
        String pass;
        String pass2;
        String firstname;
        String lastname;
        String phone;
        String job;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getPass2() {
            return pass2;
        }

        public void setPass2(String pass2) {
            this.pass2 = pass2;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getJob() {
            return job;
        }

        public void setJob(String job) {
            this.job = job;
        }


        public User(){

        }
    }
}
