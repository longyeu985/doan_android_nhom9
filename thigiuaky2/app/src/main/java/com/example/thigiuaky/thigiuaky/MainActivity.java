package com.example.thigiuaky.thigiuaky;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText edt_user;
    EditText edt_pass;
    Button btn_login;
    Button btn_cancel;
    TextView tv_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edt_user = (EditText) findViewById(R.id.edt_user);
        edt_pass = (EditText) findViewById(R.id.edt_pass);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        tv_create = (TextView)findViewById(R.id.tv_create);

        SetEvent();

    }

    public void SetEvent() {
        btn_login.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        edt_user.setOnClickListener(this);
        edt_pass.setOnClickListener(this);
        tv_create.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                String edtuser = edt_user.getText().toString().trim();
                String edtpass = edt_pass.getText().toString().trim();
                if (edtuser.equals("") && edtpass.equals("")) {
                    Intent intent = new Intent(MainActivity.this, Main3Activity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Dang nhap that bai", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btn_cancel:
                Toast.makeText(getApplicationContext(), "Ket thuc dang nhap", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_create:
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                startActivity(intent);
                break;


        }
    }
}
