package com.example.thigiuaky.thigiuaky;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ImageView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Main3Activity extends AppCompatActivity implements ValueEventListener {
    ctadapter adapterct;
    List<Country> ctlist;
    ListView lv;
    Button btn_back;
    Button btn_plus;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference  = firebaseDatabase.getReference().child("User");


        lv = (ListView)findViewById(R.id.listview1);
        ctlist = new ArrayList<>();

        adapterct = new ctadapter(this,R.layout.itemlist,ctlist);
        lv.setAdapter(adapterct);

        databaseReference.addValueEventListener(this);

        Button btn_plus = (Button)findViewById(R.id.btn_plus);
        btn_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Country country4 = new Country("Bac Phap", "myRef", R.drawable.db, R.drawable.db2, R.drawable.db3);
                Country country5 = new Country("Minh Tuan", "Chua Co Noi Dung", R.drawable.db, R.drawable.db2, R.drawable.db3);
                Country country6 = new Country("Son", "Chua Co Noi Dung", R.drawable.db, R.drawable.db2, R.drawable.db3);
                Country country7 = new Country("Son", "Chua Co Noi Dung", R.drawable.db, R.drawable.db2, R.drawable.db3);
                Country country8 = new Country("Son", "Chua Co Noi Dung", R.drawable.db, R.drawable.db2, R.drawable.db3);
                ctlist =new ArrayList<Country>();
                ctlist.add(country4);
                ctlist.add(country5);
                ctlist.add(country6);
                ctlist.add(country7);
                ctlist.add(country8);
                databaseReference.setValue(ctlist);
//                databaseReference.push().setValue(ctlist);
            }
        });
        // Read from the database
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            public static final String TAG = "";
//
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // This method is called once with the initial value and again
//                // whenever data at this location is updated.
//                String value = dataSnapshot.getValue(String.class);
//                Log.d(TAG, "Value is: " + value);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError error) {
//                // Failed to read value
//                Log.w(TAG, "Failed to read value.", error.toException());
//            }
//        });



        btn_back = (Button)findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main3Activity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        Iterable<DataSnapshot> nodeChild = dataSnapshot.getChildren();
        adapterct.clear();
        for (DataSnapshot data : nodeChild){
            Country country = data.getValue(Country.class);

            ctlist.add(country);
            adapterct.notifyDataSetChanged();
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
class Country implements Serializable {
    private String Name;
    private String Content;
    private int Avarta;
    private int icon1;
    private int icon2;

    public Country() {
    }

    public Country(String name, String content, int avarta, int icon1, int icon2) {
        Name = name;
        Content = content;
        Avarta = avarta;
        this.icon1 = icon1;
        this.icon2 = icon2;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public int getAvarta() {
        return Avarta;
    }

    public void setAvarta(int avarta) {
        Avarta = avarta;
    }

    public int getIcon1() {
        return icon1;
    }

    public void setIcon1(int icon1) {
        this.icon1 = icon1;
    }

    public int getIcon2() {
        return icon2;
    }

    public void setIcon2(int icon2) {
        this.icon2 = icon2;
    }
}

//hello